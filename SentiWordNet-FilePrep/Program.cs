﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentiWordNet_FilePrep
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage: SentiWordNet-FilePrep.exe [input] [output]");
            }

            var inputPath = args[0];
            var outputPath = args[1];

            var lines = File.ReadAllLines(inputPath);

            int longestWordLength = 0;
            string longestWord = "";

            using (var output = new FileStream(outputPath, FileMode.Create))
            {
                foreach (var line in lines)
                {
                    if (string.IsNullOrEmpty(line) || line[0] == '#')
                        continue;

                    var parts = line.Split('\t');

                    if (parts.Length != 6)
                    {
                        Console.WriteLine("Warning: incorrect number of parts: " + line);
                        continue;
                    }

                    var words = parts[4];
                    var positiveStr = parts[2];
                    var negativeStr = parts[3];

                    float positive,negative;

                    if (!float.TryParse(positiveStr, out positive)
                        || !float.TryParse(negativeStr, out negative))
                    {
                        continue;
                    }

                    var wordParts = words.Split(' ');

                    var posBytes = BitConverter.GetBytes(positive);
                    var negBytes = BitConverter.GetBytes(negative);

                    foreach (var word in wordParts)
                    {
                        var hashLocation = word.IndexOf('#');
                        var realWord = word;

                        if (hashLocation > 0)
                            realWord = word.Substring(0, hashLocation);

                        if (realWord.Contains('_'))
                            continue;

                        if (realWord.Length > longestWordLength)
                        {
                            longestWordLength = realWord.Length;
                            longestWord = realWord;
                        }

                        var wordBytes = Encoding.UTF8.GetBytes(realWord);

                        output.Write(wordBytes, 0, wordBytes.Length);
                        output.WriteByte(0x09);
                        output.Write(posBytes, 0, posBytes.Length);
                        output.Write(negBytes, 0, negBytes.Length);
                        output.WriteByte(0x0A);
                    }
                }
            }

            Console.WriteLine("Done processing. Longest word: " + longestWord + " - length " + longestWordLength);
        }

        private class SentimentSet
        {
            public string Word;
            public float Positive;
            public float Negative;
        }
    }
}
