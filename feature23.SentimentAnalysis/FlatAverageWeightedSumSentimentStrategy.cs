﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace feature23.SentimentAnalysis
{
    internal class FlatAverageWeightedSumSentimentStrategy : ISentimentStrategy
    {
        private readonly float _positiveWeight;
        private readonly float _negativeWeight;
        private readonly bool _ignoreObjectiveWords;
        private readonly List<float> _values;

        private bool _previousWordNegative = false;
        private bool _previousWordPositiveDegreeAdverb = false;

        public FlatAverageWeightedSumSentimentStrategy(float positiveWeight, float negativeWeight, bool ignoreObjectiveWords)
        {
            _positiveWeight = Math.Abs(positiveWeight);
            _negativeWeight = Math.Abs(negativeWeight);

            if (_positiveWeight == 0)
                _positiveWeight = 1;
            if (_negativeWeight == 0)
                _negativeWeight = 1;

            _ignoreObjectiveWords = ignoreObjectiveWords;
            _values = new List<float>();
        }

        public void WordMatch(string word, SentimentSet set)
        {
            // TODO: convert this to some sort of portable hashset impl
            switch (word.ToLower())
            {
                case "very":
                case "extremely":
                case "especially":
                case "particularly":
                case "ridiculously":
                case "overwhelmingly":
                case "rather":
                case "absolutely":
                case "decidedly":
                case "enormously":
                case "entirely":
                case "fully":
                case "greatly":
                case "highly":
                case "incredibly":
                case "indeed":
                case "intensely":
                case "perfectly":
                case "positively":
                case "purely":
                case "really":
                case "strongly":
                case "thoroughly":
                case "totally":
                case "utterly":
                    _previousWordPositiveDegreeAdverb = true;
                    return;
                case "too":
                case "terribly":
                case "awfully":
                case "badly":
                case "barely":
                case "hardly":
                case "scarcely":
                case "rarely":
                case "no":
                case "not":
                case "none":
                case "nobody":
                case "nothing":
                case "neither":
                case "nowhere":
                case "never":
                case "doesn't":
                case "isn't":
                case "wasn't":
                case "shouldn't":
                case "wouldn't":
                case "couldn't":
                case "won't":
                case "can't":
                case "don't":
                    _previousWordNegative = true;
                    return;
                default:
                    break;
            }

            if (_ignoreObjectiveWords && set.Positive == 0.0f && set.Negative == 0.0f)
                return;

            var calcValue = (set.Positive * _positiveWeight) + (set.Negative * -1 * _negativeWeight);

            if (_previousWordPositiveDegreeAdverb)
                calcValue = calcValue * 2; // TODO: make this configurable? or machine learn ideal value for english?

            if (_previousWordNegative)
                _values.Add(calcValue * -1);
            else
                _values.Add(calcValue);

            _previousWordNegative = false;
            _previousWordPositiveDegreeAdverb = false;
        }

        public float CalculateAverage()
        {
            if (_values.Count == 0)
                return 0.0f;

            return Math.Max(-1, Math.Min(1, _values.Average()));
        }
    }
}
