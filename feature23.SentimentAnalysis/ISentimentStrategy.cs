﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace feature23.SentimentAnalysis
{
    internal interface ISentimentStrategy
    {
        void WordMatch(string word, SentimentSet set);
    }
}
