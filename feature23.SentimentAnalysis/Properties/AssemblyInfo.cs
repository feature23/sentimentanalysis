﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("feature23.SentimentAnalysis")]
[assembly: AssemblyDescription("A simple, portable sentiment analysis library with an embedded sentiment dataset from SentiWordNet (http://sentiwordnet.isti.cnr.it/).")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("feature[23]")]
[assembly: AssemblyProduct("feature23.SentimentAnalysis")]
[assembly: AssemblyCopyright("Copyright © Paul Irwin and feature[23], 2013, except sentiment dataset from SentiWordNet (http://sentiwordnet.isti.cnr.it/), usage covered under CC BY-SA 3.0 license.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
