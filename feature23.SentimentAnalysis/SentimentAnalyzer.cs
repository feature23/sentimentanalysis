﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace feature23.SentimentAnalysis
{
    /// <summary>
    /// A basic sentiment analyzer. Note: constructor execution time can take much longer than the calculations, so 
    /// try to keep an instance alive (i.e. in a static variable) to avoid repetitive ctor hits.
    /// </summary>
    public class SentimentAnalyzer
    {
        private IDictionary<string, SentimentSet> _sentimentData;

        /// <summary>
        /// Constructs a new SentimentAnalyzer.
        /// </summary>
        public SentimentAnalyzer()
        {
            _sentimentData = new Dictionary<string, SentimentSet>(StringComparer.OrdinalIgnoreCase);

            using (var stream = this.GetType().Assembly.GetManifestResourceStream("feature23.SentimentAnalysis.SentiWordNetOut.senti"))
            {
                byte[] buffer = new byte[256];
                int b;
                int bufferpos = 0;
                byte cur;
                string word = "";
                float pos, neg;

                while (stream.Position < stream.Length - 1)
                {
                    b = stream.ReadByte();

                    if (b < 0)
                        break;

                    cur = (byte)b;

                    if (cur == 0x0A)
                    {
                        Array.Clear(buffer, 0, bufferpos);
                        bufferpos = 0;
                    }
                    else if (cur == 0x09)
                    {
                        if (bufferpos == 0)
                        {
                            Array.Clear(buffer, 0, buffer.Length);
                            bufferpos = 0;
                        }

                        // tab delimiter between word and values
                        word = Encoding.UTF8.GetString(buffer, 0, bufferpos);

                        bufferpos++; // move past tab char

                        byte[] posBytes = new byte[4], negBytes = new byte[4];

                        stream.Read(posBytes, 0, 4);
                        stream.Read(negBytes, 0, 4);

                        pos = BitConverter.ToSingle(posBytes, 0);
                        neg = BitConverter.ToSingle(negBytes, 0);

                        if (!_sentimentData.ContainsKey(word))
                        {
                            _sentimentData[word] = new SentimentSet { Positive = pos, Negative = neg };
                        }
                    }
                    else
                    {
                        buffer[bufferpos++] = cur;
                    }
                }
            }
        }

        /// <summary>
        /// Computes a flat, directional sentiment value for the given <paramref name="inputText"/>.
        /// A returned value that is positive indicates positive sentiment, negative indicates negative,
        /// and 0 indicates objective. Possible values range from 1 to -1. The "closeness to objectivity" 
        /// is up to your application to decide.
        /// </summary>
        /// <param name="inputText">The input text to analyze.</param>
        /// <returns>Returns a sentiment value from -1 to 1.</returns>
        public float ComputeFlatSentiment(string inputText)
        {
            if (string.IsNullOrEmpty(inputText))
                return 0.0f; // objective

            using (var reader = new StringReader(inputText))
            {
                return ComputeFlatSentiment(reader);
            }
        }

        /// <summary>
        /// Computes a flat, directional sentiment value for the given text <paramref name="reader"/>.
        /// A returned value that is positive indicates positive sentiment, negative indicates negative,
        /// and 0 indicates objective. Possible values range from 1 to -1. The "closeness to objectivity" 
        /// is up to your application to decide.
        /// </summary>
        /// <param name="reader">The input text stream to analyze.</param>
        /// <returns>Returns a sentiment value from -1 to 1.</returns>
        public float ComputeFlatSentiment(TextReader reader)
        {
            var strategy = new FlatAverageWeightedSumSentimentStrategy(0, 0, true);

            AnalyzeText(reader, strategy);

            return strategy.CalculateAverage();
        }

        /// <summary>
        /// Computes a flat, directional sentiment value for the given text <paramref name="reader"/>
        /// with the provided positive weight, negative weight, and whether or not to ignore objective words (score 0.0).
        /// A returned value that is positive indicates positive sentiment, negative indicates negative,
        /// and 0 indicates objective. Possible values range from 1 to -1. The "closeness to objectivity" 
        /// is up to your application to decide.
        /// </summary>
        /// <param name="inputText">The input text to analyze.</param>
        /// <param name="positiveWeight">The weight to apply (multiply) to positive scores.</param>
        /// <param name="negativeWeight">The weight to apply (multiply) to negative scores.</param>
        /// <param name="ignoreObjectiveWords">Whether or not to ignore words with a 0.0 positive and negative score.</param>
        /// <returns>Returns a sentiment value from -1 to 1.</returns>
        public float ComputeWeightedFlatSentiment(string inputText, float positiveWeight, float negativeWeight, bool ignoreObjectiveWords)
        {
            if (string.IsNullOrEmpty(inputText))
                return 0.0f; // objective

            using (var reader = new StringReader(inputText))
            {
                return ComputeWeightedFlatSentiment(reader, positiveWeight, negativeWeight, ignoreObjectiveWords);
            }
        }

        /// <summary>
        /// Computes a flat, directional sentiment value for the given text <paramref name="reader"/>
        /// with the provided positive weight, negative weight, and whether or not to ignore objective words (score 0.0).
        /// A returned value that is positive indicates positive sentiment, negative indicates negative,
        /// and 0 indicates objective. Possible values range from 1 to -1. The "closeness to objectivity" 
        /// is up to your application to decide.
        /// </summary>
        /// <param name="reader">The input reader stream to analyze.</param>
        /// <param name="positiveWeight">The weight to apply (multiply) to positive scores.</param>
        /// <param name="negativeWeight">The weight to apply (multiply) to negative scores.</param>
        /// <param name="ignoreObjectiveWords">Whether or not to ignore words with a 0.0 positive and negative score.</param>
        /// <returns>Returns a sentiment value from -1 to 1.</returns>
        public float ComputeWeightedFlatSentiment(TextReader reader, float positiveWeight, float negativeWeight, bool ignoreObjectiveWords)
        {
            var strategy = new FlatAverageWeightedSumSentimentStrategy(positiveWeight, negativeWeight, ignoreObjectiveWords);

            AnalyzeText(reader, strategy);

            return strategy.CalculateAverage();
        }

        private void AnalyzeText(TextReader reader, ISentimentStrategy strategy)
        {
            if (reader == null || reader.Peek() < 0)
                return;

            // max tested word length is 32
            char[] buffer = new char[32];
            string word = "";
            int bufferpos = 0;
            int cint;
            char c;

            while ((cint = reader.Read()) >= 0)
            {
                c = (char)cint;

                if (bufferpos < 32 && (char.IsLetter(c) || c == '-' || c == '\''))
                {
                    buffer[bufferpos++] = c;
                }
                else
                {
                    // non-word character or we hit 32 chars
                    if (bufferpos > 1) // minimum word length 2 chars
                    {
                        word = new string(buffer, 0, bufferpos);

                        SentimentSet set;

                        if (_sentimentData.TryGetValue(word, out set))
                        {
                            strategy.WordMatch(word, set);
                        }
                    }
                    
                    Array.Clear(buffer, 0, bufferpos);
                    bufferpos = 0;
                }
            }

            if (bufferpos > 1) // minimum word length 2 chars
            {
                word = new string(buffer, 0, bufferpos);

                SentimentSet set;

                if (_sentimentData.TryGetValue(word, out set))
                {
                    strategy.WordMatch(word, set);
                }
            }
        }
    }
}
