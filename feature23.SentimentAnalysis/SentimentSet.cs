﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace feature23.SentimentAnalysis
{
    public struct SentimentSet
    {
        public float Positive;
        public float Negative;

        public float Objective
        {
            get { return 1 - (Positive + Negative); }
        }
    }
}
