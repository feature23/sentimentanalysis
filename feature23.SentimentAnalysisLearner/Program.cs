﻿using feature23.SentimentAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace feature23.SentimentAnalysisLearner
{
    class Program
    {
        private static readonly SentimentAnalyzer _sentimentAnalyzer = new SentimentAnalyzer();

        private static List<AnalysisResult> _analysisResults = new List<AnalysisResult>();

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage: feature23.SentimentAnalysisLearner.exe [path]");
                return;
            }

            string path = args[0];

            if (!File.Exists(path))
            {
                Console.WriteLine("File doesn't exist!");
                return;
            }

            Console.Clear();

            using (var fileStream = new FileStream(path, FileMode.Open))
            {
                byte[] buffer = new byte[1024];
                int length = 0;
                StringBuilder sentence = new StringBuilder();

                while ((length = fileStream.Read(buffer, 0, 1024)) > 0)
                {
                    string value = Encoding.UTF8.GetString(buffer);

                    int periodIndex = value.IndexOf('.');

                    if (periodIndex == -1)
                    {
                        sentence.Append(value);
                    }
                    else
                    {
                        bool shouldBreakOuter = false;

                        while ((periodIndex = value.IndexOf('.')) >= 0)
                        {                            
                            sentence.Append(value.Substring(0, periodIndex));

                            var shouldContinue = AssessSentence(sentence.ToString());

                            if (!shouldContinue || periodIndex == value.Length - 1)
                            {
                                sentence.Clear();
                                shouldBreakOuter = true;
                                break;
                            }

                            value = value.Substring(periodIndex + 1, value.Length - periodIndex - 1);

                            sentence.Clear();
                        }

                        if (shouldBreakOuter)
                            break;
                    }
                }
            }

            Console.Clear();
            Console.WriteLine("Final analysis:");

            float avgPositive = _analysisResults.Where(i => i.Success).Average(i => i.PositiveWeight);
            float avgNegative = _analysisResults.Where(i => i.Success).Average(i => i.NegativeWeight);

            Console.WriteLine("\tSuggested positive weight: " + avgPositive.ToString("0.00"));
            Console.WriteLine("\tSuggested negative weight: " + avgNegative.ToString("0.00"));

            if (Debugger.IsAttached)
                Console.ReadKey();
        }

        private static bool AssessSentence(string sentence)
        {
            Console.WriteLine("Sentence: " + sentence + ".");
            Console.WriteLine();

            Console.WriteLine("Is this sentence [p]ositive, [n]egative, [o]bjective, [s]kip, or [q]uit?");
            var key = Console.ReadKey();

            if (key.Key == ConsoleKey.Q)
            {
                return false;
            }

            if (key.Key == ConsoleKey.P
                || key.Key == ConsoleKey.N
                || key.Key == ConsoleKey.O)
            {
                var analysisResult = new AnalysisResult()
                {
                    //Sentence = sentence
                };

                bool positiveTestExpectedResult = false;
                bool negativeTestExpectedResult = false;
                                
                if (key.Key == ConsoleKey.P)
                    positiveTestExpectedResult = true;
                else if (key.Key == ConsoleKey.N)
                    negativeTestExpectedResult = true;

                analysisResult.PositiveExpected = positiveTestExpectedResult;
                analysisResult.NegativeExpected = negativeTestExpectedResult;

                float positiveWeight = 1, negativeWeight = 1;

                float result = _sentimentAnalyzer.ComputeWeightedFlatSentiment(sentence, positiveWeight, negativeWeight, true);
                int iterations = 1;

                analysisResult.InitialResult = result;

                if ((result > 0.1f && positiveTestExpectedResult)
                    || (result < -0.1f && negativeTestExpectedResult)
                    || (!positiveTestExpectedResult && !negativeTestExpectedResult))
                {
                    analysisResult.Success = true;
                    analysisResult.Result = result;
                    analysisResult.PositiveWeight = positiveWeight;
                    analysisResult.NegativeWeight = negativeWeight;
                }
                else
                {
                    while (iterations < 10
                        && (((result > 0.1f) != positiveTestExpectedResult)
                        || ((result < -0.1f) != negativeTestExpectedResult)))
                    {
                        if (positiveTestExpectedResult)
                        {
                            positiveWeight += (float)(0.1 * iterations);
                        }
                        else if (negativeTestExpectedResult)
                        {
                            negativeWeight += (float)(0.1 * iterations);
                        }
                        else 
                        {
                            if (result > 0.1)
                                negativeWeight += (float)(0.1 * iterations);
                            else
                                positiveWeight += (float)(0.1 * iterations);
                        }

                        result = _sentimentAnalyzer.ComputeWeightedFlatSentiment(sentence, positiveWeight, negativeWeight, true);

                        iterations++;

                        if ((result > 0.1f && positiveTestExpectedResult)
                            || (result < -0.1f && negativeTestExpectedResult)
                            || (!positiveTestExpectedResult && !negativeTestExpectedResult))
                        {
                            analysisResult.Success = true;
                            analysisResult.Result = result;
                            analysisResult.Iterations = iterations;
                            analysisResult.PositiveWeight = positiveWeight;
                            analysisResult.NegativeWeight = negativeWeight;
                        }
                    }
                }

                analysisResult.Iterations = iterations;
                _analysisResults.Add(analysisResult);

                Console.Clear();
                Console.WriteLine(string.Format("Last result: {0}. {1} iterations, {2} pos weight, {3} neg weight.", analysisResult.Success ? "Successful" : "Not successful", analysisResult.Iterations, analysisResult.PositiveWeight, analysisResult.NegativeWeight));
                Console.WriteLine();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Previous sentence skipped.");
                Console.WriteLine();
            }

            return true;
        }

        private class AnalysisResult
        {
            //public string Sentence;
            public bool PositiveExpected;
            public bool NegativeExpected;
            public bool Success;
            public float PositiveWeight;
            public float NegativeWeight;
            public int Iterations;
            public float Result;
            public float InitialResult;
        }
    }
}
