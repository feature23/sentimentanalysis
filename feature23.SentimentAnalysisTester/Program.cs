﻿using feature23.SentimentAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace feature23.SentimentAnalysisTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter text to analyze: ");
            var text = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("Loading analyzer...");

            var sw = Stopwatch.StartNew();

            var analyzer = new SentimentAnalyzer();

            sw.Stop();

            Console.WriteLine("Analyzer loaded. " + sw.ElapsedMilliseconds + " ms");
            Console.WriteLine();
            Console.WriteLine("Starting default analysis...");

            sw.Reset();
            sw.Start();

            var value = analyzer.ComputeFlatSentiment(text);

            sw.Stop();
            
            Console.WriteLine("Default analysis took " + sw.ElapsedMilliseconds + " ms.");
            Console.WriteLine();
            Console.WriteLine("Default Analysis Result: " + (value == 0 ? "Objective" : (value > 0 ? "Positive" : "Negative")) + ", " + value.ToString("0.00"));
            
            Console.WriteLine();
            Console.WriteLine("Starting weighted analysis (pos * 1, neg * 3, ignore objective words)...");

            sw.Reset();
            sw.Start();

            value = analyzer.ComputeWeightedFlatSentiment(text, 1, 3, true);

            sw.Stop();

            Console.WriteLine("Weighted analysis took " + sw.ElapsedMilliseconds + " ms.");
            Console.WriteLine();
            Console.WriteLine("Weighted Analysis Result: " + (value == 0 ? "Objective" : (value > 0 ? "Positive" : "Negative")) + ", " + value.ToString("0.00"));


            if (Debugger.IsAttached)
                Console.ReadLine();
        }
    }
}
